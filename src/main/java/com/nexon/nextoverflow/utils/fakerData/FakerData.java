package com.nexon.nextoverflow.utils.fakerData;

import com.github.javafaker.Faker;
import com.nexon.nextoverflow.modules.post.dto.response.AnswerResponse;
import com.nexon.nextoverflow.modules.post.dto.response.CommentResponse;
import com.nexon.nextoverflow.modules.post.dto.response.QuestionResponse;
import com.nexon.nextoverflow.modules.tag.dto.response.TagResponse;
import com.nexon.nextoverflow.modules.tag.dto.response.TagSearchResponse;
import com.nexon.nextoverflow.modules.user.dto.response.UserResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Service
@Log4j2
@RequiredArgsConstructor
public class FakerData {
    private Faker faker = new Faker();

    public List<UserResponse> getListFakeDataUsers() {
        List<UserResponse> responseUsers = new ArrayList<UserResponse>();

        for (int i = 0; i < 15; i++) {
            var userResponse = UserResponse.builder()
                    .name(faker.name().fullName())
                    .avatarUrl(faker.internet().image())
                    .location(faker.address().city())
                    .description(faker.lorem().sentence())
                    .build();

            responseUsers.add(userResponse);
        }

        return responseUsers;
    }

    public UserResponse getUserFakeData() {
        return UserResponse.builder()
                .name(faker.name().fullName())
                .avatarUrl(faker.internet().image())
                .location(faker.address().city())
                .description(faker.lorem().sentence())
                .build();
    }

    public List<QuestionResponse> getFakeListQuestion() {
        List<QuestionResponse> listQuestion = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            List<String> tags = new ArrayList<>();
            tags.add(faker.lorem().word());
            tags.add(faker.lorem().word());
            QuestionResponse question = QuestionResponse.builder()
                    .id(i)
                    .acceptedAnswerId(faker.random().nextInt(0, 10))
                    .title(faker.lorem().sentence())
                    .isHidden(faker.random().nextInt(0, 1).byteValue())
                    .content(faker.lorem().paragraph())
                    .viewCount(faker.random().nextInt(100, 1000))
                    .voteCountUp(faker.random().nextInt(0, 100))
                    .voteCountDown(faker.random().nextInt(0, 100))
                    .createDateTime(LocalDateTime.now())
                    .modifyDateTime(LocalDateTime.now())
                    .tags(getFakeListTagSearch())
                    .creator(getUserFakeData())
                    .build();
            listQuestion.add(question);
        }
        return listQuestion;
    }

    public QuestionResponse getFakeQuestion(Integer id) {
        List<String> tags = new ArrayList<>();
        tags.add(faker.lorem().word());
        tags.add(faker.lorem().word());
        tags.add(faker.lorem().word());
        return QuestionResponse.builder()
                .id(id)
                .acceptedAnswerId(faker.random().nextInt(0, 10))
                .title(faker.lorem().sentence())
                .isHidden(faker.random().nextInt(0, 1).byteValue())
                .content(faker.lorem().paragraph())
                .viewCount(faker.random().nextInt(100, 1000))
                .voteCountUp(faker.random().nextInt(0, 100))
                .voteCountDown(faker.random().nextInt(0, 100))
                .createDateTime(LocalDateTime.now())
                .modifyDateTime(LocalDateTime.now())
                .tags(getFakeListTagSearch())
                .creator(getUserFakeData())
                .build();
    }

    public List<AnswerResponse> getFakeListAnswer(Integer questionId) {
        List<AnswerResponse> listAnswer = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            AnswerResponse answer = AnswerResponse.builder()
                    .id(i)
                    .questionId(questionId)
                    .isHidden(faker.random().nextInt(0, 1).byteValue())
                    .content(faker.lorem().paragraph())
                    .viewCount(faker.random().nextInt(100, 1000))
                    .voteCountUp(faker.random().nextInt(0, 100))
                    .voteCountDown(faker.random().nextInt(0, 100))
                    .createDateTime(LocalDateTime.now())
                    .modifyDateTime(LocalDateTime.now())
                    .creator(getUserFakeData())
                    .build();
            ;

            listAnswer.add(answer);
        }
        return listAnswer;
    }

    public List<CommentResponse> getFakeListComment(Integer postId) {
        List<CommentResponse> listComment = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            CommentResponse comment = CommentResponse.builder()
                    .id(i)
                    .postId(postId)
                    .content(faker.lorem().paragraph())
                    .createDateTime(LocalDateTime.now())
                    .modifyDateTime(LocalDateTime.now())
                    .creator(getUserFakeData())
                    .build();


            listComment.add(comment);
        }
        return listComment;
    }

    public List<TagSearchResponse> getFakeListTagSearch() {
        List<TagSearchResponse> listTag = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            TagSearchResponse tag = TagSearchResponse.builder()
                    .id(i)
                    .name(faker.lorem().word())
                    .build();
            listTag.add(tag);
        }
        return listTag;
    }

//    public List<TagFrontResponse> getFakeListTagFront() {
//        List<TagFrontResponse> listTag = new ArrayList<>();
//        for (int i = 0; i < 15; i++) {
//            TagFrontResponse tag = TagFrontResponse.builder()
//                    .id(i)
//                    .name(faker.lorem().word())
//                    .questionCount(faker.random().nextInt(0, 100))
//                    .description(faker.lorem().sentence())
//                    .build();
//
//            listTag.add(tag);
//        }
//        return listTag;
//    }

//    public TagResponse getFakeTag(Integer id) {
//
//        return TagResponse.builder()
//                .id(id)
//                .name(faker.lorem().word())
//                .questionCount(faker.random().nextInt(0, 100))
//                .description(faker.lorem().sentence())
//                .questions(getFakeListQuestion())
//                .build();
//    }


}
