package com.nexon.nextoverflow.utils;

import com.nexon.nextoverflow.exception.ApiException;
import com.nexon.nextoverflow.exception.ExceptionEnum;
import com.nexon.nextoverflow.modules.post.entity.VoteType;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class CommonUtil {
    public static VoteType isValidVoteType(String vote) {
        try {
            return VoteType.valueOf(vote.toUpperCase());
        } catch (IllegalArgumentException e) {
            log.error("Invalid vote type");
            throw new ApiException(ExceptionEnum.BAD_REQUEST, "Invalid vote type");
        }
    }
}
