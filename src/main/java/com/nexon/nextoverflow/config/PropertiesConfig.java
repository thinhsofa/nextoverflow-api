package com.nexon.nextoverflow.config;

import jakarta.annotation.PostConstruct;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Getter
@Log4j2
@Configuration
public class PropertiesConfig {

    @Value("${spring.profiles.active:local}")
    private String activeProfile;

    private final Environment environment;


    /******************* Secret *****************/
    @Value("${secret-key.DATABASE_USERNAME:}")
    private String username;

    @Value("${secret-key.DATABASE_PASSWORD:}")
    private String password;

    public PropertiesConfig(Environment environment) {
        this.environment = environment;
    }

    @PostConstruct
    public void init() {

        if (!activeProfile.equals("local")) {
          // do something at env live
        }

        log.info("========================== Property Info ==========================");
        log.info("-- active profile: {}", activeProfile);

        log.info("=================================================================\n");

    }
}
