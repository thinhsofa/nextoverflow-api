package com.nexon.nextoverflow.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import javax.sql.DataSource;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Log4j2
@RequiredArgsConstructor
public class DataSourceConfig {

    private final PropertiesConfig propertiesConfig;


    private Integer MB_MAX_LIFE_TIME = 1200000;

    private Integer MB_CONNECTION_TIMEOUT = 20000;

    private Integer MB_IDLE_TIMEOUT = 300000;

    @Value("${spring.datasource.url}")
    private String dburl;

    @Value("${spring.datasource.pool-size}")
    private int poolSize;


    @Bean
    public DataSource dataSource() {
        HikariConfig dataSourceConfig;
        dataSourceConfig = new HikariConfig();
        dataSourceConfig.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSourceConfig.setJdbcUrl(dburl);
        dataSourceConfig.setUsername(propertiesConfig.getUsername());
        dataSourceConfig.setPassword(propertiesConfig.getPassword());
        dataSourceConfig.setMaxLifetime(MB_MAX_LIFE_TIME);
        dataSourceConfig.setConnectionTimeout(MB_CONNECTION_TIMEOUT);
        dataSourceConfig.setIdleTimeout(MB_IDLE_TIMEOUT);
        dataSourceConfig.setMaximumPoolSize(poolSize);


        log.info("========================== DB Info ==========================");
        log.info("-- jdbc url: {}", dataSourceConfig.getJdbcUrl());
        log.info("-- driver class name: {}", dataSourceConfig.getDriverClassName());
        log.info("-- max pool size: {}", dataSourceConfig.getMaximumPoolSize());
        log.info("=================================================================\n");

        return new HikariDataSource(dataSourceConfig);
    }
}
