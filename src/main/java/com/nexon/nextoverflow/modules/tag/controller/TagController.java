package com.nexon.nextoverflow.modules.tag.controller;

import com.nexon.nextoverflow.common.annotaion.AuditLog;
import com.nexon.nextoverflow.common.aspectOrientProgramming.dto.ActionType;
import com.nexon.nextoverflow.common.constant.AppConstants;
import com.nexon.nextoverflow.common.dto.ResponseDto;
import com.nexon.nextoverflow.modules.tag.dto.request.TagRequest;
import com.nexon.nextoverflow.modules.tag.service.TagService;
import com.nexon.nextoverflow.utils.PaginationAndSortFactory;
import io.swagger.annotations.Api;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@Log4j2
@RequiredArgsConstructor
@RestController
@Api(value = "/tag", tags = "Tag", description = "Tag API")

public class TagController {
    private final TagService tagService;

    @AuditLog(type = ActionType.SEARCH)
    @GetMapping("/v1/tags")
    public ResponseDto<?> getTagList() {
        log.info("req > get list tag");
        var response = tagService.getTags();
        log.info("res > get list tag");
        return ResponseDto.ok(response);
    }

    @AuditLog(type = ActionType.SEARCH)
    @GetMapping("v1/tags-front")
    public ResponseDto<?> getTagFrontList(@RequestParam(defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer pageNo,
                                          @RequestParam(defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer sizePerPage,
                                          @RequestParam(defaultValue = AppConstants.DEFAULT_SORT_BY) String sortBy,
                                          @RequestParam(defaultValue = AppConstants.DEFAULT_SORT_DIRECTION) String sortDirection) {
        log.info("req > get list tag front");
        Pageable pageable = PaginationAndSortFactory.getPagable(pageNo, sizePerPage, sortBy, sortDirection);

        var response = tagService.getTagFrontResponses(pageable);
        log.info("res > get list tag front");
        return ResponseDto.ok(response);
    }

    @AuditLog(type = ActionType.SEARCH)
    @GetMapping("v1/tags/{tagId}")
    public ResponseDto<?> getTagById(@PathVariable Integer tagId) {
        log.info("req > get tag by id");
        var response = tagService.getTagById(tagId);
        log.info("res > get tag by id");
        return ResponseDto.ok(response);
    }
    @AuditLog(type = ActionType.SEARCH)
    @GetMapping("v1/tags/search")
    public ResponseDto<?> searchTag(@RequestParam (defaultValue = "") String name) {
        log.info("req > search tag");
        var response = tagService.getTagSearchResponses(name);
        log.info("res > search tag");
        return ResponseDto.ok(response);
    }

    @AuditLog(type = ActionType.ADD)
    @PostMapping("/v1/tags")
    public ResponseDto<?> saveTag(@RequestBody @Valid TagRequest request) {
        log.info("req > post tag request: {}", request);
        var response = tagService.saveTag(request);
        log.info("res > post tag");
        return ResponseDto.ok(response);
    }

    @AuditLog(type = ActionType.MODIFY)
    @PutMapping("/v1/tags/{tagId}")
    public ResponseDto<?> updateTag(@PathVariable Integer tagId,
                                    @RequestBody @Valid TagRequest request) {
        log.info("req > put tag request: {}", request);
        var response = tagService.updateTag(tagId, request);
        log.info("res > put tag");
        return ResponseDto.ok(response);
    }

    @AuditLog(type = ActionType.DELETE)
    @DeleteMapping("/v1/tags/{tagId}")
    public ResponseDto<?> deleteTag(@PathVariable Integer tagId) {
        log.info("req > delete tag");
        var response = tagService.deleteTag(tagId);
        log.info("res > delete tag");
        return ResponseDto.ok(response);
    }

}
