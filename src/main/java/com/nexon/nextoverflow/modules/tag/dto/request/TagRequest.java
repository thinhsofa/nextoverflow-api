package com.nexon.nextoverflow.modules.tag.dto.request;

import com.nexon.nextoverflow.modules.tag.entity.Tag;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import lombok.extern.log4j.Log4j2;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Log4j2
public class TagRequest {
    @NotNull
    private String name;
    @NotNull
    private String description;

    public Tag toEntity(TagRequest tagRequest) {
        return Tag.builder()
                .name(name)
                .description(description)
                .viewCount(0)
                .questionCount(0)
                .build();
    }


}
