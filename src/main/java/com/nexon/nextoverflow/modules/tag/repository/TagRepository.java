package com.nexon.nextoverflow.modules.tag.repository;

import com.nexon.nextoverflow.modules.tag.entity.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TagRepository extends JpaRepository<Tag, Integer> {

    @Query(nativeQuery = true,
            value = "SELECT * FROM tag"
                    + " WHERE name like %:name%")
    List<Tag> searchName(@Param("name")String name);

    Page<Tag> findAll(Pageable pageable);

    Page<Tag> findAllTagsById(Integer id, Pageable pageable);

}
