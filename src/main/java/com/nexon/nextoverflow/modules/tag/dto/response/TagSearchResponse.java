package com.nexon.nextoverflow.modules.tag.dto.response;

import com.nexon.nextoverflow.modules.tag.entity.Tag;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TagSearchResponse {
    private Integer id;
    private String name;

    public TagSearchResponse(Tag tag) {
        this.id = tag.getId();
        this.name = tag.getName();
    }
}
