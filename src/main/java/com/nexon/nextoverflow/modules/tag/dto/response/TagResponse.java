package com.nexon.nextoverflow.modules.tag.dto.response;

import com.nexon.nextoverflow.modules.post.dto.response.QuestionResponse;
import com.nexon.nextoverflow.modules.tag.entity.Tag;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TagResponse {
    private Integer id;
    private String name;
    private String description;
    private Integer questionCount;
    private Integer viewCount;

    public TagResponse(Tag entity){
        this.id = entity.getId();
        this.name = entity.getName();
        this.description = entity.getDescription();
        this.questionCount = entity.getQuestionCount();
        this.viewCount = entity.getViewCount();
    }
}
