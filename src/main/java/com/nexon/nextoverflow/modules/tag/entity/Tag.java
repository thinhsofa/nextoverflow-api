package com.nexon.nextoverflow.modules.tag.entity;


import com.nexon.nextoverflow.modules.post.entity.Post;
import com.nexon.nextoverflow.modules.tag.dto.request.TagRequest;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.Arrays;
import java.util.List;

@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "tag")
@SuperBuilder
public class Tag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotNull
    @Size(max = 155)
    private String name;
    @NotNull
    @Column(columnDefinition = "TEXT")
    private String description;
    private Integer viewCount;
    private Integer questionCount;

    @Builder.Default
    @ManyToMany(mappedBy = "tagList")
    private List<Post> questionList = Arrays.asList();

    public void update(TagRequest request){
        this.name = request.getName();
        this.description = request.getDescription();
    }

}
