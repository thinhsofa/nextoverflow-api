package com.nexon.nextoverflow.modules.tag.service;

import com.nexon.nextoverflow.common.dto.PaginationResponse;
import com.nexon.nextoverflow.exception.ApiException;
import com.nexon.nextoverflow.exception.ExceptionEnum;
import com.nexon.nextoverflow.modules.tag.dto.request.TagRequest;
import com.nexon.nextoverflow.modules.tag.dto.response.TagResponse;
import com.nexon.nextoverflow.modules.tag.dto.response.TagSearchResponse;

import com.nexon.nextoverflow.modules.tag.entity.Tag;
import com.nexon.nextoverflow.modules.tag.repository.TagRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Log4j2
@RequiredArgsConstructor
public class TagService {
    private final TagRepository tagRepository;

    @Transactional(readOnly = true)
    public List<TagResponse> getTags() {
        List<TagResponse> responses = new ArrayList<>();
        List<Tag> tags = tagRepository.findAll();
        responses = tags.stream().map(TagResponse::new).toList();
        return responses;
    }

    public Tag getTagEntity(Integer tagId) {
        return tagRepository.findById(tagId).orElseThrow(() -> {
            log.error("not found id: {}", tagId);
            return new ApiException(ExceptionEnum.NOT_FOUND,
                    String.format("not found id: %d", tagId));
        });

    }

    @Transactional(readOnly = true)
    public TagResponse getTagResponse(Integer tagId) {
        Tag tag = getTagEntity(tagId);
        return new TagResponse(tag);
    }

    @Transactional(readOnly = true)
    public List<TagSearchResponse> getTagSearchResponses(String name) {
        List<TagSearchResponse> responses = new ArrayList<>();
        List<Tag> tags = tagRepository.searchName(name);
        responses = tags.stream().map(TagSearchResponse::new).toList();
        return responses;
    }

    @Transactional(readOnly = true)
    public PaginationResponse getTagFrontResponses(Pageable pageable) {
        Page<Tag> tags = tagRepository.findAll(pageable);
        List<TagResponse> tagResponseList = tags
                .stream().map(TagResponse::new)
                .collect(Collectors.toList());
        return PaginationResponse.builder()
                .content(tagResponseList)
                .totalPages(tags.getTotalPages())
                .totalElements(tags.getTotalElements())
                .build();
    }

    @Transactional(readOnly = true)
    public TagResponse getTagById(Integer tagId) {
        Tag tag = getTagEntity(tagId);
        return new TagResponse(tag);
    }

    @Transactional
    public TagResponse saveTag(TagRequest request) {
        Tag tag = request.toEntity(request);
        tagRepository.save(tag);
        return new TagResponse(tag);
    }

    @Transactional
    public TagResponse updateTag(Integer tagId, TagRequest request) {
        Tag tag = getTagEntity(tagId);
        tag.update(request);
        return new TagResponse(tag);
    }

    @Transactional
    public TagResponse deleteTag(Integer tagId) {
        Tag tag = getTagEntity(tagId);
        if (tag.getQuestionList().size() > 0){
            log.error("tag has question: {}", tagId);
            throw new ApiException(ExceptionEnum.BAD_REQUEST,
                    String.format("tag has question: %d", tagId));
        }
        tagRepository.delete(tag);
        return new TagResponse(tag);
    }



}
