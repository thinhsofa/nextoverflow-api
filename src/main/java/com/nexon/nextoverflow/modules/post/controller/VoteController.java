package com.nexon.nextoverflow.modules.post.controller;

import com.nexon.nextoverflow.common.annotaion.AuditLog;
import com.nexon.nextoverflow.common.aspectOrientProgramming.dto.ActionType;
import com.nexon.nextoverflow.common.dto.ResponseDto;
import com.nexon.nextoverflow.modules.post.dto.request.VoteRequest;
import com.nexon.nextoverflow.modules.post.service.VoteService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;

@Log4j2
@RequiredArgsConstructor
@RestController
@Api(value = "/vote", tags = "Vote", description = "Vote API")
public class VoteController {
    private final VoteService voteService;

    @AuditLog(type = ActionType.SEARCH)
    @PostMapping("/v1/vote/{postId}")
    public ResponseDto<?> vote(@PathVariable Integer postId, @RequestBody VoteRequest type) {
        log.info("req > vote up");
        var response = voteService.vote(postId, type);
        log.info("req > vote up");
        return ResponseDto.ok(response);
    }

    @AuditLog(type = ActionType.DELETE)
    @DeleteMapping("/v1/vote/{postId}")
    public ResponseDto<?> cancelVote(@PathVariable Integer postId) {
        log.info("req > cancel vote");
        var response = voteService.cancelVote(postId);
        log.info("req > cancel vote");
        return ResponseDto.ok(response);
    }


}
