package com.nexon.nextoverflow.modules.post.dto.response;

import com.nexon.nextoverflow.modules.user.dto.response.UserResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CommentResponse {
    private Integer id;
    private Integer postId;
    private String content;
    private LocalDateTime createDateTime;
    private LocalDateTime modifyDateTime;
    private UserResponse creator;

}
