package com.nexon.nextoverflow.modules.post.entity;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.stream.Stream;

public enum VoteType {
    VOTE_UP("VOTE_UP"),
    VOTE_DOWN("VOTE_DOWN");
    private final String voteType;
    VoteType(String voteType) {
        this.voteType = voteType;
    }
    public String getVoteType() {
        return voteType;
    }
    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public static VoteType findByCode(String code) {
        return Stream.of(VoteType.values())
                .filter(c -> c.voteType.equals(code))
                .findFirst()
                .orElse(null);
    }

}
