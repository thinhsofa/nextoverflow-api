package com.nexon.nextoverflow.modules.post.dto.request;

import com.nexon.nextoverflow.modules.post.entity.Post;
import com.nexon.nextoverflow.modules.post.entity.PostType;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Log4j2
public class QuestionRequest {
    @NotNull
    private String title;
    @NotNull
    private String content;
    @Builder.Default
    private List<Integer> tagIdList = Arrays.asList();

    public Post toEntity() {
        return Post.builder()
                .title(title)
                .content(content)
                .isHidden((byte) 0)
                .postType(PostType.QUESTION)
                .viewCount(0)
                .voteCountUp(0)
                .voteCountDown(0)
                .creator(null)
                .build();
    }
}
