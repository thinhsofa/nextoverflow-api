package com.nexon.nextoverflow.modules.post.entity;

import com.nexon.nextoverflow.common.entity.BaseEntity;
import com.nexon.nextoverflow.modules.post.dto.request.QuestionRequest;
import com.nexon.nextoverflow.modules.tag.entity.Tag;
import com.nexon.nextoverflow.modules.user.entity.User;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "posts")
@SuperBuilder
public class Post extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="parent_id")
    private Post parent;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="accept_answer_id")
    private Post acceptedAnswer;

    @Enumerated(EnumType.STRING)
    @Column(name="post_type")
    private PostType postType;

    @NotNull(message = "0:false, 1:true")
    private Byte isHidden;

    @Size(max = 1000)
    private String title;

    @Column(columnDefinition = "MEDIUMTEXT")
    private String content;

    @Builder.Default
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "post_tag",
            joinColumns = @JoinColumn(name = "post_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id"))
    private List<Tag> tagList = Arrays.asList();

    @ManyToOne(targetEntity = User.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "creator_id")
    private User creator;

    private Integer viewCount;

    @OneToMany(mappedBy = "post", cascade = CascadeType.ALL, orphanRemoval = true)
    @Builder.Default
    private Set<Vote> votes = new HashSet<>();

    private Integer voteCountUp;
    private Integer voteCountDown;
    public void update(QuestionRequest request){
        this.title = request.getTitle();
        this.content = request.getContent();
    }

    public void setIsHidden(Byte isHidden) {
        this.isHidden = isHidden;
    }

    public void setTagList (List<Tag> tagList){
        this.tagList = tagList;
    }

    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    public void setVoteCountUp(Integer voteCountUp) {
        this.voteCountUp = voteCountUp;
    }

    public void setVoteCountDown(Integer voteCountDown) {
        this.voteCountDown = voteCountDown;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }
}
