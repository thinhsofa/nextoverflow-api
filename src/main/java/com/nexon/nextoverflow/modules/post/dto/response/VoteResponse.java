package com.nexon.nextoverflow.modules.post.dto.response;

import com.nexon.nextoverflow.modules.post.entity.Vote;
import lombok.Getter;

@Getter
public class VoteResponse {
    private int id;
    private int postId;
    private int totalVoteCountUp;
    private int totalVoteCountDown;

    public VoteResponse(Vote vote) {
        id = vote.getId();
        postId = vote.getPost().getId();
        totalVoteCountUp = vote.getPost().getVoteCountUp();
        totalVoteCountDown = vote.getPost().getVoteCountDown();
    }


}
