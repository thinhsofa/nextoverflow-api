package com.nexon.nextoverflow.modules.post.controller;


import com.nexon.nextoverflow.common.annotaion.AuditLog;
import com.nexon.nextoverflow.common.aspectOrientProgramming.dto.ActionType;
import com.nexon.nextoverflow.common.constant.AppConstants;
import com.nexon.nextoverflow.common.dto.ResponseDto;
import com.nexon.nextoverflow.common.entity.Pagination;
import com.nexon.nextoverflow.modules.post.service.AnswerService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;

@Log4j2
@RequiredArgsConstructor
@RestController
@Api(value = "/answer", tags = "Answer", description = "Answer API")
@RequestMapping("/v1/question")
public class AnswerController {
    private final AnswerService answerService;

    @AuditLog(type = ActionType.SEARCH)
    @GetMapping("/{questionId}/answer")
    public ResponseDto<?> getAnswerList(@PathVariable Integer questionId,
                                        @RequestParam(defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer pageNo,
                                        @RequestParam(defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer sizePerPage,
                                        @RequestParam(defaultValue = AppConstants.DEFAULT_SORT_DIRECTION) String sortBy) {
        log.info("req > get list answer");
        Pagination pagination = Pagination.builder()
                .pageNo(pageNo)
                .sortBy(sortBy)
                .sizePerPage(sizePerPage)
                .build();
        var response = answerService.getAnswerList(questionId, pagination);
        log.info("req > get list answer");
        return ResponseDto.ok(response);
    }


}
