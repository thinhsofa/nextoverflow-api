package com.nexon.nextoverflow.modules.post.controller;


import com.nexon.nextoverflow.common.annotaion.AuditLog;
import com.nexon.nextoverflow.common.aspectOrientProgramming.dto.ActionType;
import com.nexon.nextoverflow.common.constant.AppConstants;
import com.nexon.nextoverflow.common.dto.ResponseDto;
import com.nexon.nextoverflow.modules.post.dto.request.QuestionRequest;
import com.nexon.nextoverflow.modules.post.service.QuestionService;
import com.nexon.nextoverflow.utils.PaginationAndSortFactory;
import io.swagger.annotations.Api;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@Log4j2
@RequiredArgsConstructor
@RestController
@Api(value = "/question", tags = "Question", description = "Question API")
public class QuestionController {
    private final QuestionService questionService;

    @AuditLog(type = ActionType.SEARCH)
    @GetMapping("/v1/question")
    public ResponseDto<?> getQuestionList(@RequestParam(required = false, defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer pageNo,
                                          @RequestParam(required = false, defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer sizePerPage,
                                          @RequestParam(required = false, defaultValue = AppConstants.DEFAULT_SORT_BY) String sortBy,
                                          @RequestParam(required = false, defaultValue = AppConstants.DEFAULT_SORT_DIRECTION) String sortDirection) {
        log.info("req > get list question");

        Pageable pageable = PaginationAndSortFactory.getPagable(pageNo, sizePerPage, sortBy, sortDirection);

        var responseDto = questionService.getList(pageable);
        log.info("req > get list question");
        return ResponseDto.ok(responseDto);
    }

    @AuditLog(type = ActionType.SEARCH)
    @GetMapping("/v1/question-front")
    public ResponseDto<?> getQuestionListFront(@RequestParam(defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer pageNo,
                                               @RequestParam(defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer sizePerPage,
                                               @RequestParam(defaultValue = AppConstants.DEFAULT_SORT_BY) String sortBy,
                                               @RequestParam(defaultValue = AppConstants.DEFAULT_SORT_DIRECTION) String sortDirection,
                                               @RequestParam(required = false) String searchKeyWord) {
        log.info("req > get list question");

        Pageable pageable = PaginationAndSortFactory.getPagable(pageNo, sizePerPage, sortBy, sortDirection);
        var responseDto = questionService.getListFront(pageable, searchKeyWord);
        log.info("req > get list question");
        return ResponseDto.ok(responseDto);
    }

    @AuditLog(type = ActionType.SEARCH)
    @GetMapping("/v1/question-front/{questionId}")
    public ResponseDto<?> getQuestionFront(@PathVariable Integer questionId) {
        log.info("req > get question id: {}", questionId);
        var responseDto = questionService.getQuestionFront(questionId);
        log.info("req > get question id: {}", questionId);
        return ResponseDto.ok(responseDto);
    }

    @AuditLog(type = ActionType.SEARCH)
    @GetMapping("/v1/question-front/tags/{tagId}")
    public ResponseDto<?> getQuestionListByTagId(@PathVariable Integer tagId,
                                                 @RequestParam(defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer pageNo,
                                                 @RequestParam(defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer sizePerPage,
                                                 @RequestParam(defaultValue = AppConstants.DEFAULT_SORT_BY) String sortBy,
                                                 @RequestParam(defaultValue = AppConstants.DEFAULT_SORT_DIRECTION) String sortDirection) {
        log.info("req > get list question");

        Pageable pageable = PaginationAndSortFactory.getPagable(pageNo, sizePerPage, sortBy, sortDirection);

        var responseDto = questionService.getListFrontByTagId(tagId, pageable);
        log.info("req > get list question");
        return ResponseDto.ok(responseDto);
    }


    @AuditLog(type = ActionType.SEARCH)
    @GetMapping("v1/question/{questionId}")
    public ResponseDto<?> getQuestion(@PathVariable Integer questionId) {
        log.info("req > get question id: {}", questionId);
        var responseDto = questionService.getQuestion(questionId);
        log.info("req > get question id: {}", questionId);
        return ResponseDto.ok(responseDto);
    }

    @AuditLog(type = ActionType.ADD)
    @PostMapping("/v1/question")
    public ResponseDto<?> save(@RequestBody @Valid QuestionRequest request) {
        log.info("req > post question body: {}", request.toString());
        var response = questionService.save(request);
        log.info("res > post question");
        return ResponseDto.ok(response);
    }

    @AuditLog(type = ActionType.MODIFY)
    @PutMapping("/v1/question/{questionId}")
    public ResponseDto<?> update(@PathVariable Integer questionId, @RequestBody @Valid QuestionRequest request) {
        log.info("req > put question id: {}, body: {}", questionId, request.toString());
        var response = questionService.update(questionId, request);
        log.info("res > put question id: {}", questionId);
        return ResponseDto.ok(response);
    }
    @AuditLog(type = ActionType.MODIFY)
    @PutMapping("/v1/question/{questionId}/view")
    public ResponseDto<?> updateView(@PathVariable Integer questionId) {
        log.info("req > put question id: {}", questionId);
        var response = questionService.updateViewCount(questionId);
        log.info("res > put question id: {}", questionId);
        return ResponseDto.ok(response);
    }

    @AuditLog(type = ActionType.DELETE)
    @DeleteMapping("/v1/question/{questionId}")
    public ResponseDto<?> delete(@PathVariable Integer questionId) {
        log.info("req > delete question id: {}", questionId);
        var response = questionService.delete(questionId);
        log.info("res > delete question id: {}", questionId);
        return ResponseDto.ok(response);
    }
}
