package com.nexon.nextoverflow.modules.post.dto.response;

import com.nexon.nextoverflow.modules.post.entity.Post;
import com.nexon.nextoverflow.modules.tag.dto.response.TagSearchResponse;
import com.nexon.nextoverflow.modules.user.dto.response.UserResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;
@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class QuestionResponse {
    private Integer id;
    private Integer acceptedAnswerId;
    private String title;
    private Byte isHidden;
    private String content;
    private Integer viewCount;
    private Integer voteCountUp;
    private Integer voteCountDown;
    private LocalDateTime createDateTime;
    private LocalDateTime modifyDateTime;
    private List<TagSearchResponse> tags;
    private UserResponse creator;

    public QuestionResponse(Post post, UserResponse creator) {
        this.id = post.getId();
        if (post.getAcceptedAnswer() != null)
            this.acceptedAnswerId = post.getAcceptedAnswer().getId();
        this.title = post.getTitle();
        this.isHidden = post.getIsHidden();
        this.content = post.getContent();
        this.viewCount = post.getViewCount();
        this.voteCountUp = post.getVoteCountUp();
        this.voteCountDown = post.getVoteCountDown();
        this.createDateTime = post.getCreatedDate();
        this.modifyDateTime = post.getModifiedDate();
        this.tags = post.getTagList().stream().map(TagSearchResponse::new).toList();
        this.creator = creator;
    }
}
