package com.nexon.nextoverflow.modules.post.service;

import com.nexon.nextoverflow.common.dto.PaginationResponse;
import com.nexon.nextoverflow.exception.ApiAdvancedException;
import com.nexon.nextoverflow.exception.ApiException;
import com.nexon.nextoverflow.exception.ApiMeta;
import com.nexon.nextoverflow.exception.ExceptionEnum;
import com.nexon.nextoverflow.modules.post.dto.request.QuestionRequest;
import com.nexon.nextoverflow.modules.post.dto.response.QuestionResponse;
import com.nexon.nextoverflow.modules.post.entity.Post;
import com.nexon.nextoverflow.modules.post.entity.PostType;
import com.nexon.nextoverflow.modules.post.repository.PostRepository;
import com.nexon.nextoverflow.modules.tag.entity.Tag;
import com.nexon.nextoverflow.modules.tag.service.TagService;
import com.nexon.nextoverflow.modules.user.dto.response.UserResponse;
import com.nexon.nextoverflow.modules.user.entity.User;
import com.nexon.nextoverflow.modules.user.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Log4j2
@RequiredArgsConstructor
public class QuestionService {
    private final PostRepository postRepository;
    private final TagService tagService;
    private final UserService userService;

    private final int userId = 1;

    @Transactional
    public QuestionResponse save(QuestionRequest request) {
        Post question = request.toEntity();
        List<Tag> tagList = new ArrayList<>();
        for (var tagId : request.getTagIdList()) {
            Tag tag = tagService.getTagEntity(tagId);
            tagList.add(tag);
        }
        question.setTagList(tagList);

        User user = userService.getUserEntity(userId);
        question.setCreator(user);
        postRepository.save(question);
        return new QuestionResponse(question, new UserResponse(user));
    }

    @Transactional(readOnly = true)
    public PaginationResponse getList(Pageable pageable) {
        Page<Post> questionList = postRepository
                .findAllByPostType(PostType.QUESTION, pageable);
        List<QuestionResponse> questionResponseList = questionList
                .stream().map(question -> new QuestionResponse(question, new UserResponse(question.getCreator())))
                .collect(Collectors.toList());

        return PaginationResponse.builder()
                .content(questionResponseList)
                .totalPages(questionList.getTotalPages())
                .totalElements(questionList.getTotalElements())
                .build();
    }

    @Transactional(readOnly = true)
    public PaginationResponse getListFront(Pageable pageable, String searchKeyWord) {
        Page<Post> questionList = postRepository
                .findAllPostFront(searchKeyWord, pageable);
        List<QuestionResponse> questionResponseList = questionList
                .stream().map(question -> new QuestionResponse(question, new UserResponse(question.getCreator())))
                .collect(Collectors.toList());
        return PaginationResponse.builder()
                .content(questionResponseList)
                .totalPages(questionList.getTotalPages())
                .totalElements(questionList.getTotalElements())
                .build();
    }

    @Transactional(readOnly = true)
    public PaginationResponse getListFrontByTagId(Integer tagId, Pageable pageable) {
        Page<Post> questionList = postRepository
                .findAllByPostTypeAndTagListId(PostType.QUESTION, tagId, pageable);
        List<QuestionResponse> questionResponseList = questionList
                .stream().map(question -> new QuestionResponse(question, new UserResponse(question.getCreator())))
                .collect(Collectors.toList());
        return PaginationResponse.builder()
                .content(questionResponseList)
                .totalPages(questionList.getTotalPages())
                .totalElements(questionList.getTotalElements())
                .build();
    }

    @Transactional(readOnly = true)
    public QuestionResponse getQuestionFront(Integer questionId) {
        Post question = findQuestionByQuestionId(questionId);
        if (question.getIsHidden() == 1) {
            throw new ApiAdvancedException(ExceptionEnum.FORBIDDEN, "blog, hidden is true",
                    new ApiMeta(true));
        }
        UserResponse userResponse = userService.getUser(question.getCreator().getId());
        return new QuestionResponse(question, userResponse);
    }

    @Transactional(readOnly = true)
    public QuestionResponse getQuestion(Integer questionId) {
        Post question = findQuestionByQuestionId(questionId);
        return new QuestionResponse(question, new UserResponse(question.getCreator()));
    }

    @Transactional
    public QuestionResponse update(Integer questionId, QuestionRequest request) {
        Post question = findQuestionByQuestionId(questionId);
        question.update(request);
        return new QuestionResponse(question, new UserResponse(question.getCreator()));
    }

    @Transactional
    public QuestionResponse updateViewCount(Integer questionId) {
        Post question = findQuestionByQuestionId(questionId);
        question.setViewCount(question.getViewCount() + 1);
        return new QuestionResponse(question, new UserResponse(question.getCreator()));
    }

    @Transactional
    public QuestionResponse delete(Integer questionId) {
        Post question = findQuestionByQuestionId(questionId);
        question.setIsHidden((byte) 1);
        return new QuestionResponse(question, new UserResponse(question.getCreator()));
    }


    private Post findQuestionByQuestionId(Integer questionId) {
        return postRepository.findByPostTypeAndId(PostType.QUESTION, questionId)
                .orElseThrow(() -> {
                    log.error("Question not found. questionId: {}", questionId);
                    return new ApiException(ExceptionEnum.NOT_FOUND,
                            String.format("Question not found. questionId: %d", questionId));
                });
    }


}
