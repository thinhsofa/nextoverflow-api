package com.nexon.nextoverflow.modules.post.repository;

import com.nexon.nextoverflow.modules.post.entity.Vote;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface VoteRepository extends JpaRepository<Vote, Integer> {
    Optional<Vote> findByPostIdAndUserId(int postId, int userId);
}
