package com.nexon.nextoverflow.modules.post.service;

import com.nexon.nextoverflow.common.entity.Pagination;
import com.nexon.nextoverflow.modules.post.dto.response.CommentResponse;
import com.nexon.nextoverflow.utils.fakerData.FakerData;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Log4j2
@RequiredArgsConstructor
public class CommentService {
    private final FakerData fakerData;
    public List<CommentResponse> getList(Integer postId, Pagination pagination) {
        return fakerData.getFakeListComment(postId);
    }
}
