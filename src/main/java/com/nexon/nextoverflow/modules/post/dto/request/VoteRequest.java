package com.nexon.nextoverflow.modules.post.dto.request;

import lombok.Getter;

@Getter
public class VoteRequest {
    private String voteType;
}
