package com.nexon.nextoverflow.modules.post.controller;

import com.nexon.nextoverflow.common.annotaion.AuditLog;
import com.nexon.nextoverflow.common.aspectOrientProgramming.dto.ActionType;
import com.nexon.nextoverflow.common.constant.AppConstants;
import com.nexon.nextoverflow.common.dto.ResponseDto;
import com.nexon.nextoverflow.common.entity.Pagination;
import com.nexon.nextoverflow.modules.post.service.CommentService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;

@Log4j2
@RequiredArgsConstructor
@RestController
@Api(value = "/comment", tags = "Comment", description = "Comment API")
@RequestMapping("/v1/question")
public class CommentController {
    private final CommentService commentService;

    @AuditLog(type = ActionType.SEARCH)
    @GetMapping("/{questionId}/comment")
    public ResponseDto<?> getCommentList(@PathVariable Integer questionId,
                                         @RequestParam(defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer pageNo,
                                         @RequestParam(defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer sizePerPage,
                                         @RequestParam(defaultValue = AppConstants.DEFAULT_SORT_DIRECTION) String sortBy) {
        log.info("req > get list comment");
        Pagination pagination = Pagination.builder()
                .pageNo(pageNo)
                .sortBy(sortBy)
                .sizePerPage(sizePerPage)
                .build();
        var response = commentService.getList(questionId, pagination);
        log.info("res > get list comment");
        return ResponseDto.ok(response);
    }

}
