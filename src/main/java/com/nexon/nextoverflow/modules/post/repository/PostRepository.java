package com.nexon.nextoverflow.modules.post.repository;

import com.nexon.nextoverflow.common.entity.Pagination;
import com.nexon.nextoverflow.modules.post.entity.Post;
import com.nexon.nextoverflow.modules.post.entity.PostType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

public interface PostRepository extends JpaRepository<Post, Integer> {

    Page<Post> findAllByPostType(PostType postType, Pageable pageable);
    Page<Post> findAllByPostTypeAndIsHidden(PostType postType, Byte isHidden,Pageable pageable);

    Page<Post> findAllByPostTypeAndTagListId(PostType postType, Integer tagId, Pageable pageable);

    Optional<Post> findByPostTypeAndId(PostType postType, Integer id);

    @Query(value = "SELECT DISTINCT p.id, p.title, p.content, p.parent_id, p.accept_answer_id, p.post_type, p.is_hidden, p.view_count, p.vote_count_up, p.vote_count_down, p.created_date, p.modified_date, p.creator_id FROM posts p " +
            "LEFT JOIN post_tag pt ON p.id = pt.post_id " +
            "LEFT JOIN tag t ON pt.tag_id = t.id " +
            "WHERE p.post_type = 'QUESTION' AND p.is_hidden = 0 " +
            "AND ((:keyword IS NULL OR p.title LIKE CONCAT('%', :keyword, '%')) " +
            "OR (:keyword IS NULL OR t.name LIKE CONCAT('%', :keyword, '%')) " +
            "OR (:keyword IS NULL OR p.content LIKE CONCAT('%', :keyword, '%')))",
            countQuery = "SELECT COUNT(DISTINCT p.id) FROM posts p " +
                    "LEFT JOIN post_tag pt ON p.id = pt.post_id " +
                    "LEFT JOIN tag t ON pt.tag_id = t.id " +
                    "WHERE p.post_type = 'QUESTION' AND p.is_hidden = 0 " +
                    "AND ((:keyword IS NULL OR p.title LIKE CONCAT('%', :keyword, '%')) " +
                    "OR (:keyword IS NULL OR t.name LIKE CONCAT('%', :keyword, '%')) " +
                    "OR (:keyword IS NULL OR p.content LIKE CONCAT('%', :keyword, '%')))",
            nativeQuery = true)
    Page<Post> findAllPostFront(@Param("keyword") String keyword, Pageable pageable);



}
