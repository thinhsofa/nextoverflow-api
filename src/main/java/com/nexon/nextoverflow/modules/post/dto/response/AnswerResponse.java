package com.nexon.nextoverflow.modules.post.dto.response;

import com.nexon.nextoverflow.modules.user.dto.response.UserResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class AnswerResponse {
    private Integer id;
    private Integer questionId;
    private Byte isHidden;
    private String content;
    private Integer viewCount;
    private Integer voteCountUp;
    private Integer voteCountDown;
    private LocalDateTime createDateTime;
    private LocalDateTime modifyDateTime;
    private UserResponse creator;

}
