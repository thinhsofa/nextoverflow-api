package com.nexon.nextoverflow.modules.post.service;

import com.nexon.nextoverflow.exception.ApiException;
import com.nexon.nextoverflow.exception.ExceptionEnum;
import com.nexon.nextoverflow.modules.post.dto.request.VoteRequest;
import com.nexon.nextoverflow.modules.post.dto.response.VoteResponse;
import com.nexon.nextoverflow.modules.post.entity.Post;
import com.nexon.nextoverflow.modules.post.entity.Vote;
import com.nexon.nextoverflow.modules.post.entity.VoteType;
import com.nexon.nextoverflow.modules.post.repository.PostRepository;
import com.nexon.nextoverflow.modules.post.repository.VoteRepository;
import com.nexon.nextoverflow.modules.user.entity.User;
import com.nexon.nextoverflow.modules.user.service.UserService;
import com.nexon.nextoverflow.utils.CommonUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Log4j2
@RequiredArgsConstructor
public class VoteService {
    private final VoteRepository voteRepository;
    private final PostRepository postRepository;
    private final UserService userService;
    private final Integer userId = 1;

    private Post getPost(Integer postId) {
        return postRepository.findById(postId).orElseThrow(
                () -> {
                    log.error("Post not found");
                    return new ApiException(ExceptionEnum.NOT_FOUND, "Post not found");
                }
        );
    }

    @Transactional
    public VoteResponse vote(Integer postId, VoteRequest type) {
        VoteType voteType = CommonUtil.isValidVoteType(type.getVoteType());
        Optional<Vote> duplicateVote = voteRepository.findByPostIdAndUserId(postId, userId);
        if (duplicateVote.isPresent()) {
            log.error("Duplicate vote");
            throw new ApiException(ExceptionEnum.BAD_REQUEST, "Duplicate vote");
        }
        Post post = getPost(postId);
        User user = userService.getUserEntity(userId);
        Vote vote = Vote.builder()
                .post(post)
                .voteType(voteType)
                .user(user)
                .build();
        voteRepository.save(vote);
        if (voteType == VoteType.VOTE_UP) {
            post.setVoteCountUp(post.getVoteCountUp() + 1);
        } else {
            post.setVoteCountDown(post.getVoteCountDown() + 1);
        }
        return new VoteResponse(vote);
    }


    @Transactional
    public VoteResponse cancelVote(Integer postId) {
        Vote vote = voteRepository.findByPostIdAndUserId(postId, userId).orElseThrow(
                () -> {
                    log.error("Vote not found");
                    return new ApiException(ExceptionEnum.NOT_FOUND, "Vote not found");
                }
        );
        Post post = getPost(postId);
        User user = userService.getUserEntity(userId);
        if (vote.getVoteType() == VoteType.VOTE_UP) {
            post.setVoteCountUp(post.getVoteCountUp() - 1);
        } else {
            post.setVoteCountDown(post.getVoteCountDown() - 1);
        }
        voteRepository.delete(vote);
        return new VoteResponse(vote);
    }

}
