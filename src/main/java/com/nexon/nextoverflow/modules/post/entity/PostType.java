package com.nexon.nextoverflow.modules.post.entity;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.stream.Stream;

public enum PostType {
    QUESTION("question"),
    ANSWER("answer"),
;
    private final String postType;
    PostType(String postType) {
        this.postType = postType;
    }
    public String getPostType() {
        return postType;
    }

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public static PostType findByCode(String code) {
        return Stream.of(PostType.values())
                .filter(c -> c.postType.equals(code))
                .findFirst()
                .orElse(null);
    }

}
