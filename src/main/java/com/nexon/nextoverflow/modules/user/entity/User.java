package com.nexon.nextoverflow.modules.user.entity;

import com.nexon.nextoverflow.common.entity.BaseEntity;
import com.nexon.nextoverflow.modules.post.entity.Post;
import com.nexon.nextoverflow.modules.post.entity.Vote;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "user")
public class User extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotNull
    @Size(max = 200)
    private String displayName;

    private String avatarUrl;
    private String location;
    private String description;
    @OneToMany(mappedBy = "creator")
    private Set<Post> posts = new HashSet<>();

    @OneToMany(mappedBy = "user")
    private Set<Vote> votes = new HashSet<>();
}
