package com.nexon.nextoverflow.modules.user.controller;

import com.nexon.nextoverflow.common.annotaion.AuditLog;
import com.nexon.nextoverflow.common.aspectOrientProgramming.dto.ActionType;
import com.nexon.nextoverflow.common.constant.AppConstants;
import com.nexon.nextoverflow.common.dto.ResponseDto;
import com.nexon.nextoverflow.common.entity.Pagination;
import com.nexon.nextoverflow.modules.user.service.UserService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RequiredArgsConstructor
@RestController
@Api(value = "/users", tags = "Users", description = "Users API")
public class UserController {

    private final UserService userService;

    @AuditLog(type = ActionType.SEARCH)
    @GetMapping("/v1/users")
    public ResponseDto<?> getList(@RequestParam(defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer pageNo,
                                  @RequestParam(defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer sizePerPage,
                                  @RequestParam(defaultValue = AppConstants.DEFAULT_SORT_DIRECTION) String sortBy
    ) {
        log.info("req > get list user");
        Pagination pagination = Pagination.builder()
                .pageNo(pageNo)
                .sortBy(sortBy)
                .sizePerPage(sizePerPage)
                .build();
        var response = userService.getList(pagination);
        log.info("res > get list user");
        return ResponseDto.ok(response);
    }

    @AuditLog(type = ActionType.SEARCH)
    @GetMapping("/v1/users/{userId}")
    public ResponseDto<?> getUser(@PathVariable Integer userId) {
        log.info("req > get user");
        var response = userService.getUser(userId);
        log.info("res > get user");
        return ResponseDto.ok(response);
    }


}
