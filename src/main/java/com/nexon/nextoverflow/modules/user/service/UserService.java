package com.nexon.nextoverflow.modules.user.service;

import com.nexon.nextoverflow.common.entity.Pagination;
import com.nexon.nextoverflow.exception.ApiException;
import com.nexon.nextoverflow.exception.ExceptionEnum;
import com.nexon.nextoverflow.modules.user.dto.response.UserResponse;
import com.nexon.nextoverflow.modules.user.entity.User;
import com.nexon.nextoverflow.modules.user.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
@Log4j2
public class UserService {

    private final UserRepository userRepository;

    @Transactional(readOnly = true)
    public List<UserResponse> getList(Pagination pagination) {
        return userRepository.findAll().stream().map(UserResponse::new).collect(Collectors.toList());
    }
    @Transactional(readOnly = true)
    public User getUserEntity(Integer id) {
        return userRepository.findById(id).orElseThrow(() -> {
            log.error("accountId: {} not found", id);
            return new ApiException(ExceptionEnum.NOT_FOUND,
                    String.format("accountId: %d not found", id));
        });
    }


    public UserResponse getUser(Integer id) {
        return new UserResponse(getUserEntity(id));
    }

    public Map<Integer, UserResponse> getUserMap(List<Integer> userIdList) {
        List<UserResponse> userResponseList = userIdList.stream().map(this::getUser).collect(Collectors.toList());
        return userResponseList.stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toMap(UserResponse::getId, Function.identity()));

    }


}
