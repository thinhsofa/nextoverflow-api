package com.nexon.nextoverflow.modules.user.repository;

import com.nexon.nextoverflow.modules.user.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {
}
