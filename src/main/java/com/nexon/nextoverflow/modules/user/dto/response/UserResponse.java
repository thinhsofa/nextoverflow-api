package com.nexon.nextoverflow.modules.user.dto.response;

import com.nexon.nextoverflow.modules.user.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UserResponse {
    private Integer id;

    private String name;

    private String avatarUrl;

    private String location;

    private String description;


    public UserResponse(User user) {
        this.id = user.getId();
        this.name = user.getDisplayName();
        this.avatarUrl = user.getAvatarUrl();
        this.location = user.getLocation();
        this.description = user.getDescription();
    }

}
