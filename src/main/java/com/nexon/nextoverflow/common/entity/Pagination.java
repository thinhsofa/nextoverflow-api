package com.nexon.nextoverflow.common.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Pagination {
    private Integer pageNo;

    private Integer sizePerPage;

    private String sortBy;

}
