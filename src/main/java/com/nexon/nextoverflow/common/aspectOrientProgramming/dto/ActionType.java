package com.nexon.nextoverflow.common.aspectOrientProgramming.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ActionType {
    SEARCH("search"),
    ADD("add"),
    MODIFY("modify"),
    DELETE("delete"),
    UPLOAD("upload"),
    DOWNLOAD("download"),
    ON("on"),
    OFF("off"),
    EXECUTE("execute"),
    CANCEL("cancel"),
    APPROVAL("approval"),
    REJECT("reject"),
    BAN("ban"),
    KICK("kick"),
    DEPLOY("deploy");

    private final String action;
}
