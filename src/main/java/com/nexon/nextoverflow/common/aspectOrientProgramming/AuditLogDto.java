package com.nexon.nextoverflow.common.aspectOrientProgramming;

import lombok.Data;

@Data
public class AuditLogDto {

    private String evt_time;
    private String src_ip;
    private String src_username;
    private String component;
    private String action_item;
    private String action;
    private Object action_detail;
    private String action_result;
    private String req_uri;
    private String req_domain;
    private String service_code;
}
