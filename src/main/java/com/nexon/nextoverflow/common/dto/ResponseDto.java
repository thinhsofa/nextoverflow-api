package com.nexon.nextoverflow.common.dto;


import lombok.Getter;

@Getter
public class ResponseDto<T> {

    private final Boolean success;
    private final String message;
    private final T data;

    public ResponseDto() {
        this.success = false;
        this.message = null;
        this.data = null;
    }

    public ResponseDto(boolean success) {
        this.success = success;
        this.data = null;
        this.message = null;
    }

    public ResponseDto(boolean success, T data) {
        this.success = success;
        this.data = data;
        this.message = null;
    }

    public ResponseDto(boolean success, String message) {
        this.success = success;
        this.data = null;
        this.message = message;
    }

    public static ResponseDto<?> ok() {
        return new ResponseDto<>(true);
    }

    public static <T> ResponseDto<T> ok(T data) {
        return new ResponseDto<>(true, data);
    }

    public static ResponseDto ok(boolean success, String message) {
        return new ResponseDto(success, message);
    }
}
