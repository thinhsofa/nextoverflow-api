package com.nexon.nextoverflow.common.annotaion;

import com.nexon.nextoverflow.common.aspectOrientProgramming.dto.ActionType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface AuditLog {

    ActionType type();
}


