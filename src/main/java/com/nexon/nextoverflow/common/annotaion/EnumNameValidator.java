package com.nexon.nextoverflow.common.annotaion;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;


public class EnumNameValidator implements ConstraintValidator<EnumName, Enum<?>> {

    private EnumName annotation;

    @Override
    public void initialize(EnumName constratintAnnotation) {
        this.annotation = constratintAnnotation;
    }

    @Override
    public boolean isValid(Enum<?> value, ConstraintValidatorContext context) {
        boolean result = false;

        if (value == null) {
            return result;
        }

        Object[] enumValues = this.annotation.enumClass().getEnumConstants();
        if (enumValues != null) {
            for (Object enumValue : enumValues) {
                if (value.equals(enumValue.toString())
                    || (this.annotation.ignoreCase()
                    && value.toString().equalsIgnoreCase(enumValue.toString()))) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }
}
