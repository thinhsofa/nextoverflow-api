package com.nexon.nextoverflow.exception;

import lombok.extern.log4j.Log4j2;
import org.apache.catalina.connector.ClientAbortException;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import springfox.documentation.annotations.ApiIgnore;

import java.lang.reflect.InvocationTargetException;
import java.util.stream.Collectors;

@RestControllerAdvice
@Log4j2
@ApiIgnore
public class ApiExceptionAdvice {

    @ExceptionHandler({ApiException.class})
    public ResponseEntity<ApiExceptionEntity> exceptionHandler(final ApiException e) {
        log.error(">>> ApiException: {}", e.toString());

        return ResponseEntity.status(e.getError().getStatus())
            .body(ApiExceptionEntity.message(e.getErrorMessage()));
    }

    @ExceptionHandler({ApiAdvancedException.class})
    public ResponseEntity<ApiAdvancedExceptionEntity> exceptionHandler(final ApiAdvancedException e) {
        log.error(">>> ApiAdvancedException: {}", e.toString());

        return ResponseEntity.status(e.getError().getStatus())
            .body(ApiAdvancedExceptionEntity.message(e.getErrorMessage(), e.getApiMeta()));
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<?> exceptionHandler(final Exception e) {
        log.error(">>> Exception: {}", e.toString());
        log.error(">>> Exception all: {}", e);
        log.error(">>> Exception msg: {}", e.getLocalizedMessage());
        return ResponseEntity.status(ExceptionEnum.INTERNAL_SERVER_ERROR.getStatus())
            .body(ApiExceptionEntity.message(ExceptionEnum.INTERNAL_SERVER_ERROR.getMessage()));
    }

    @ExceptionHandler({InvocationTargetException.class})
    public ResponseEntity<?> exceptionHandler(final InvocationTargetException e) {
        log.error(">>> InvocationTargetException: {}", e.toString());
        log.error(">>> InvocationTargetException all: {}", e);
        log.error(">>> InvocationTargetException msg: {}", e.getLocalizedMessage());
        return ResponseEntity.status(ExceptionEnum.INTERNAL_SERVER_ERROR.getStatus())
            .body(ApiExceptionEntity.message(ExceptionEnum.INTERNAL_SERVER_ERROR.getMessage()));
    }

    @ExceptionHandler({RuntimeException.class})
    public ResponseEntity<ApiExceptionEntity> exceptionHandler(final RuntimeException e) {
        log.error(">>> RuntimeException: {}", e);
        return ResponseEntity.status(ExceptionEnum.INTERNAL_SERVER_ERROR.getStatus())
            .body(ApiExceptionEntity.message(ExceptionEnum.INTERNAL_SERVER_ERROR.getMessage()
                + " - " + " RuntimeException"));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<?> exceptionHandler(final MethodArgumentNotValidException e) {
        log.error(">>> MethodArgumentNotValidException: {}", e.toString());
        String errorCodes = e.getBindingResult().getAllErrors()
            .stream()
            .map(error -> error.getCodes()[0])
            .collect(Collectors.joining("."));
        log.error("- code: {}", errorCodes);

        String errorMessage = e.getBindingResult().getAllErrors()
            .stream()
            .map(error -> error.getDefaultMessage())
            .collect(Collectors.joining("."));
        log.error("- msg: {}", errorMessage);

        return ResponseEntity.status(ExceptionEnum.BAD_REQUEST.getStatus())
            .body(ApiExceptionEntity.message(ExceptionEnum.BAD_REQUEST.getMessage()
                + " - " + errorMessage));
    }

    @ExceptionHandler({MissingPathVariableException.class})
    public ResponseEntity<?> exceptionHandler(final MissingPathVariableException e) {
        log.error(">>> MissingPathVariableException: {}", e.getMessage());

        return ResponseEntity.status(ExceptionEnum.INTERNAL_SERVER_ERROR.getStatus())
            .body(ApiExceptionEntity.builder().message(e.getMessage())
                .build());
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<?> exceptionHandler(final HttpMessageNotReadableException e) {
        log.error(">>> HttpMessageNotReadableException: {}", e.getMessage());

        return ResponseEntity.status(ExceptionEnum.BAD_REQUEST.getStatus())
            .body(ApiExceptionEntity.builder().message(e.getMessage()).build());
    }

    @ExceptionHandler({ClientAbortException.class})
    public void exceptionHandler(final ClientAbortException e) {
        log.warn(">>> ClientAbortException: {}", e.getLocalizedMessage());
    }

}

