package com.nexon.nextoverflow.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ApiAdvancedException extends RuntimeException {

    private ExceptionEnum error;
    private String extraMessage;
    private ApiMeta apiMeta;

    @Override
    public String toString() {
        return error.getStatus() + ", "
            + error.getMessage() + " - "
            + extraMessage;
    }

    public String getErrorMessage() {
        if (extraMessage.length() > 0)
            return error.getMessage() + " - "
                + extraMessage;
        return error.getMessage();

    }
}
