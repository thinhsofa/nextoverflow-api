package com.nexon.nextoverflow.exception;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class ApiExceptionEntity {
    private Boolean success;
    private String message;

    @Builder
    public ApiExceptionEntity(String message) {
        this.success = false;
        this.message = message;
    }

    public static ApiExceptionEntity message(String message) {
        return new ApiExceptionEntity(message);
    }
}

