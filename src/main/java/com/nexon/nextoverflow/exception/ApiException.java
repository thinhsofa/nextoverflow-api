package com.nexon.nextoverflow.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ApiException extends RuntimeException {

    private ExceptionEnum error;
    private String extraMessage;

    public ApiException(ExceptionEnum error) {
        this.error = error;
    }

    @Override
    public String toString() {

        if (extraMessage == null) {
            return error.getStatus() + ", "
                    + error.getMessage();
        }

        return error.getStatus() + ", "
                + error.getMessage() + " - "
                + extraMessage;
    }

    public String getErrorMessage() {
        if (extraMessage != null && extraMessage.length() > 0)
            return error.getMessage() + " - "
                    + extraMessage;
        return error.getMessage();

    }
}
