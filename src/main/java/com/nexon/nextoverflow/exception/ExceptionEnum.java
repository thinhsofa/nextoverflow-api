package com.nexon.nextoverflow.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

@Getter
@ToString
@AllArgsConstructor
public enum ExceptionEnum {
    BAD_REQUEST(HttpStatus.BAD_REQUEST, "bad request"),
    FORBIDDEN(HttpStatus.FORBIDDEN, "forbidden"),
    NOT_FOUND(HttpStatus.NOT_FOUND, "not found data"),
    GONE(HttpStatus.GONE, "gone"),
    REQUEST_TIMEOUT(HttpStatus.REQUEST_TIMEOUT, "request timeout"),
    INTERNAL_SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "internal server error");

    private final HttpStatus status;
    private String message;

}

