package com.nexon.nextoverflow.exception;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class ApiAdvancedExceptionEntity {
    private Boolean success;
    private String message;
    private ApiMeta apiMeta;

    @Builder
    public ApiAdvancedExceptionEntity(String message, ApiMeta apiMeta) {
        this.success = false;
        this.message = message;
        this.apiMeta = apiMeta;
    }

    public static ApiAdvancedExceptionEntity message(String message, ApiMeta apiMeta) {
        return new ApiAdvancedExceptionEntity(message, apiMeta);
    }
}

