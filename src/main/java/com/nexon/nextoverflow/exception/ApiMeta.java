package com.nexon.nextoverflow.exception;

import lombok.Getter;

@Getter
public class ApiMeta {
    private Boolean isHidden;
    private Integer status;

    public ApiMeta(Boolean isHidden){
        this.isHidden = isHidden;
    }
}
