FROM gradle:8-alpine as gradleimage
ARG PROFILE
ENV PROFILE=$PROFILE
COPY . /home/gradle/source
WORKDIR /home/gradle/source
CMD ["cat", "./src/main/resources/application-local.yml"]
RUN gradle build -x test

FROM openjdk:17-alpine
ARG PROFILE
ENV PROFILE=$PROFILE
COPY --from=gradleimage /home/gradle/source/build/libs/nextoverflow-0.0.1-SNAPSHOT.jar /app/
WORKDIR /app
ENTRYPOINT exec java -jar -Dspring.profiles.active=$PROFILE nextoverflow-0.0.1-SNAPSHOT.jar

