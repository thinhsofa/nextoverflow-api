

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
DROP DATABASE IF EXISTS `nexoverflow` ;
CREATE database nexoverflow;
USE `nexoverflow`;


SET SQL_SAFE_UPDATES = 0;

CREATE TABLE user (
    id INT NOT NULL AUTO_INCREMENT,
    email VARCHAR(255) NOT NULL,
    password VARCHAR(255) DEFAULT NULL,
    display_name VARCHAR(200) NOT NULL,
    avatar_url TEXT NOT NULL,
    location VARCHAR(200) DEFAULT NULL,
    description TEXT DEFAULT NULL,
    role ENUM('ADMIN', 'USER') NOT NULL,
    provider ENUM('GOOGLE', 'GITHUB') DEFAULT NULL,
    created_date DATETIME(6) DEFAULT NULL,
    modified_date DATETIME(6) DEFAULT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1;


CREATE TABLE posts (
    id INT NOT NULL AUTO_INCREMENT,
    parent_id INT NULL,
    accept_answer_id INT NULL,
    creator_id INT NOT NULL,
    post_type ENUM('ANSWER','QUESTION') NOT NULL,
    title VARCHAR(100) NULL,
    is_hidden tinyint DEFAULT NULL,
    content mediumtext NOT NULL,
    view_count INT NOT NULL,
    created_date datetime(6) DEFAULT NULL,
    modified_date datetime(6) DEFAULT NULL,
    vote_count_up INT NOT NULL,
    vote_count_down INT NOT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1;

CREATE TABLE vote (
    id INT NOT NULL AUTO_INCREMENT,
    post_id INT NOT NULL,
    user_id INT NOT NULL,
    vote_type ENUM('VOTE_UP','VOTE_DOWN') NOT NULL,
    created_date datetime(6) DEFAULT NULL,
    modified_date datetime(6) DEFAULT NULL,
    PRIMARY KEY (id)

)ENGINE=InnoDB AUTO_INCREMENT=1;

CREATE TABLE favorite_group (
    id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    name_group VARCHAR(100) NULL,
    PRIMARY KEY (id)
)ENGINE=InnoDB AUTO_INCREMENT=1;

CREATE TABLE favorite_group_post (
    post_id INT NOT NULL,
    favorite_group_id INT NOT NULL,
    PRIMARY KEY (favorite_group_id, post_id)
)ENGINE=InnoDB AUTO_INCREMENT=1;

CREATE TABLE history (
    id INT NOT NULL AUTO_INCREMENT,
    post_id INT NOT NULL,
    user_id INT NOT NULL,
    content mediumtext NOT NULL,
    created_date datetime(6) DEFAULT NULL,
    PRIMARY KEY (id)
)ENGINE=InnoDB AUTO_INCREMENT=1;

CREATE TABLE tag (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(155) NOT NULL,
    description TEXT NOT NULL,
    view_count INT NOT NULL,
    question_count INT NOT NULL,
    PRIMARY KEY (id)
)ENGINE=InnoDB AUTO_INCREMENT=1;

CREATE TABLE post_tag (
    post_id INT NOT NULL,
    tag_id INT NOT NULL,
    PRIMARY KEY (post_id, tag_id)
)ENGINE=InnoDB AUTO_INCREMENT=1;

CREATE TABLE comment (
    id INT NOT NULL AUTO_INCREMENT,
    post_id INT NOT NULL,
    user_id INT NOT NULL,
    content TEXT NOT NULL,
    created_date datetime(6) DEFAULT NULL,
    modified_date datetime(6) DEFAULT NULL,
    PRIMARY KEY (id)
)ENGINE=InnoDB AUTO_INCREMENT=1;


-- Add foreign key to 'posts' table referencing 'user' table
ALTER TABLE posts
ADD CONSTRAINT fk_posts_creator_id
FOREIGN KEY (creator_id) REFERENCES user(id);

-- Add foreign key to 'posts' table referencing itself ('parent_id')
ALTER TABLE posts
ADD CONSTRAINT fk_posts_parent_id
FOREIGN KEY (parent_id) REFERENCES posts(id);

-- Add foreign key to 'posts' table referencing itself ('accept_answer_id')
ALTER TABLE posts
ADD CONSTRAINT fk_posts_accept_answer_id
FOREIGN KEY (accept_answer_id) REFERENCES posts(id);

-- Add foreign key to 'vote' table referencing 'posts' table
ALTER TABLE vote
ADD CONSTRAINT fk_vote_post_id
FOREIGN KEY (post_id) REFERENCES posts(id);

-- Add foreign key to 'vote' table referencing 'user' table
ALTER TABLE vote
ADD CONSTRAINT fk_vote_user_id
FOREIGN KEY (user_id) REFERENCES user(id);

-- Add foreign key to 'favorite_group' table referencing 'user' table
ALTER TABLE favorite_group
ADD CONSTRAINT fk_favorite_group_user_id
FOREIGN KEY (user_id) REFERENCES user(id);

-- Add foreign key to 'favorite_group_post' table referencing 'favorite_group' table
ALTER TABLE favorite_group_post
ADD CONSTRAINT fk_favorite_group_post_id
FOREIGN KEY (post_id) REFERENCES posts(id);

-- Add foreign key to 'favorite_group_post' table referencing 'favorite_group' table
ALTER TABLE favorite_group_post
ADD CONSTRAINT fk_favorite_group_group_id
FOREIGN KEY (favorite_group_id) REFERENCES favorite_group(id);

-- Add foreign key to 'history' table referencing 'posts' table
ALTER TABLE history
ADD CONSTRAINT fk_history_post_id
FOREIGN KEY (post_id) REFERENCES posts(id);

-- Add foreign key to 'history' table referencing 'user' table
ALTER TABLE history
ADD CONSTRAINT fk_history_user_id
FOREIGN KEY (user_id) REFERENCES user(id);


-- Add foreign key to 'post_tag' table referencing 'posts' table
ALTER TABLE post_tag
ADD CONSTRAINT fk_post_tag_post_id
FOREIGN KEY (post_id) REFERENCES posts(id);

-- Add foreign key to 'post_tag' table referencing 'tag' table
ALTER TABLE post_tag
ADD CONSTRAINT fk_post_tag_tag_id
FOREIGN KEY (tag_id) REFERENCES tag(id);

-- Add foreign key to 'comment' table referencing 'posts' table
ALTER TABLE comment
ADD CONSTRAINT fk_comment_post_id
FOREIGN KEY (post_id) REFERENCES posts(id);

-- Add foreign key to 'comment' table referencing 'user' table
ALTER TABLE comment
ADD CONSTRAINT fk_comment_user_id
FOREIGN KEY (user_id) REFERENCES user(id);


-- Index for parent_id field in the posts table
CREATE INDEX idx_posts_parent_id ON posts (parent_id);

-- Index for accept_answer_id field in the posts table
CREATE INDEX idx_posts_accept_answer_id ON posts (accept_answer_id);

-- Index for creator_id field in the posts table
CREATE INDEX idx_posts_creator_id ON posts (creator_id);

-- Index for tag_id field in the post_tag table
CREATE INDEX idx_post_tag_tag_id ON post_tag (tag_id);

-- Index for post_id field in the comment table
CREATE INDEX idx_comment_post_id ON comment (post_id);

-- Index for user_id field in the comment table
CREATE INDEX idx_comment_user_id ON comment (user_id);

-- Trigger for checking parent_id field in the posts table
CREATE TRIGGER check_question_parent_id_title_insert_trigger
BEFORE INSERT ON posts
FOR EACH ROW
BEGIN
    IF NEW.post_type = 'question' AND (NEW.parent_id IS NOT NULL OR NEW.title IS NULL) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot insert row with parent_id for question post or with null title.';
    END IF;

    IF NEW.post_type = 'answer' THEN
        SET NEW.accept_answer_id = NULL;
        IF NEW.parent_id IS NULL THEN
            SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot insert row without parent_id for answer post.';
        END IF;
    END IF;
END;

CREATE TRIGGER check_question_parent_id_title_update_trigger
BEFORE UPDATE ON posts
FOR EACH ROW
BEGIN
    IF NEW.post_type = 'question' AND (NEW.parent_id IS NOT NULL OR NEW.title IS NULL) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot update row with parent_id for question post or with null title.';
    END IF;

    IF NEW.post_type = 'answer' AND NEW.parent_id IS NULL THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot update row without parent_id for answer post.';
    END IF;
END;


COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;