INSERT INTO user (email, password, display_name, avatar_url, location, description, role, provider, created_date, modified_date)
VALUES
  ('user1@example.com', 'password1', 'User 1', 'avatar1.jpg', 'Location 1', 'Description 1', 'USER', 'GOOGLE', NOW(), NOW()),
  ('user2@example.com', 'password2', 'User 2', 'avatar2.jpg', 'Location 2', 'Description 2', 'USER', 'GITHUB', NOW(), NOW()),
  ('user3@example.com', 'password3', 'User 3', 'avatar3.jpg', 'Location 3', 'Description 3', 'USER', 'GOOGLE', NOW(), NOW()),
  ('user4@example.com', 'password4', 'User 4', 'avatar4.jpg', 'Location 4', 'Description 4', 'USER', 'GITHUB', NOW(), NOW()),
  ('user5@example.com', 'password5', 'User 5', 'avatar5.jpg', 'Location 5', 'Description 5', 'USER', 'GOOGLE', NOW(), NOW());


INSERT INTO posts (parent_id, accept_answer_id, creator_id, post_type, title, is_hidden, content, view_count, created_date, modified_date, vote_count_up, vote_count_down)
VALUES
  (NULL, NULL, 1, 'QUESTION', 'Post 1', 0, 'Content 1', 10, NOW(), NOW(), 5, 2),
  (1, NULL, 2, 'ANSWER', NULL, 0, 'Content 2', 5, NOW(), NOW(), 3, 1),
  (1, NULL, 3, 'ANSWER', NULL, 0, 'Content 3', 2, NOW(), NOW(), 1, 0),
  (1, NULL, 4, 'ANSWER', NULL, 0, 'Content 4', 1, NOW(), NOW(), 0, 2),
  (NULL, NULL, 5, 'QUESTION', 'Post 2', 0, 'Content 5', 8, NOW(), NOW(), 4, 1);

INSERT INTO tag (name, description, view_count, question_count)
VALUES
  ('Tag 1', 'Description 1', 20, 5),
  ('Tag 2', 'Description 2', 15, 3),
  ('Tag 3', 'Description 3', 12, 2),
  ('Tag 4', 'Description 4', 8, 4),
  ('Tag 5', 'Description 5', 5, 1);

INSERT INTO post_tag (post_id, tag_id)
VALUES
  (1, 1),
  (1, 2),
  (2, 3),
  (3, 1),
  (4, 4),
  (5, 2);
